package main

import (
	"flag"
	"log"
	"os"
	"path"

	"gitlab.com/deniskamazur/remuchi/networking"
)

var (
	trackPath string
	address   string
	logging   bool
)

func init() {
	currPath, _ := os.Getwd()
	flag.StringVar(&trackPath, "trackPath", path.Join(currPath, "/tracks/"), "path to audio tracks")

	flag.StringVar(&address, "address", ":12345", "server address")

	flag.BoolVar(&logging, "logging", false, "if logging inside command functions should be used")

	flag.Parse()
}

func main() {
	// ensure existance of filepath
	networking.EnsureDirectory(trackPath)

	device := networking.StartDevice(address)

	// command receiving-execution loop
	for command := range device.Commands {
		log.Printf("Received command %s\n", command)

		handler, found := command2Handler[command.Name]
		if !found {
			log.Printf("ERROR: command %s not implemented\n", command.Name)
		}

		err := handler(command.Info)
		if err != nil {
			log.Printf("ERROR: %s\n", err.Error())
		}
	}
}
