package main

import (
	"fmt"
	"log"
	"os"
	"path"
	"strings"

	"gitlab.com/deniskamazur/remuchi/networking"
)

type commandHandler func(info interface{}) error

var command2Handler = map[networking.CommandName]commandHandler{
	// track downloading handler
	networking.DownloadTrack: func(info interface{}) error {
		url, ok := info.(string)
		if !ok {
			log.Println("interface: ", info)
			return fmt.Errorf("corrupt interface")
		}

		urlParts := strings.Split(url, "/")
		filename := path.Join(trackPath, urlParts[len(urlParts)-1])

		if _, err := os.Stat(filename); !os.IsNotExist(err) {
			return nil
		}

		if logging {
			log.Printf("==> downloading file %s", filename)
		}

		err := DownloadFile(filename, url)

		return err
	},

	// track running handler
	networking.RunTrack: func(info interface{}) error {
		data, ok := info.(map[string]interface{})
		if !ok {
			return fmt.Errorf("corrupt interface %v", info)
		}

		trackFname, ok := data["fname"]
		if !ok {
			return fmt.Errorf("invalid data")
		}

		fname, ok := trackFname.(string)
		if !ok {
			return fmt.Errorf("corrup interface %v", fname)
		}

		buffer, err := readFileClosingBuffer(path.Join(trackPath, fname))
		if err != nil {
			return err
		}

		_, err = playSound(buffer)

		return err
	},
}
