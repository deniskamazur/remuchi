package main

import (
	"bytes"
	"io"
	"io/ioutil"
	"time"

	"github.com/faiface/beep"
	"github.com/faiface/beep/mp3"
	"github.com/faiface/beep/speaker"
)

// ClosingBuffer is a buffer that closes
type ClosingBuffer struct {
	*bytes.Buffer
}

// Close closes buffer
func (cb *ClosingBuffer) Close() (err error) {
	return
}

// Copy copies buffer
func (cb *ClosingBuffer) Copy() *ClosingBuffer {
	return &ClosingBuffer{bytes.NewBuffer(cb.Bytes())}
}

func readFileClosingBuffer(filename string) (*ClosingBuffer, error) {
	fileBytes, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}

	return &ClosingBuffer{bytes.NewBuffer(fileBytes)}, nil
}

func playSound(f io.ReadCloser) (chan struct{}, error) {
	done := make(chan struct{}, 1)

	s, format, err := mp3.Decode(f)
	if err != nil {
		close(done)
		return done, err
	}

	speaker.Init(format.SampleRate, format.SampleRate.N(time.Second/10))
	speaker.Play(beep.Seq(s, beep.Callback(func() {
		time.Sleep(time.Millisecond * 100)
		done <- struct{}{}
	})))

	return done, nil
}
