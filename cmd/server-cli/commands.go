package main

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/deniskamazur/remuchi/networking"
)

type commandHandler func(manager *networking.DeviceManager, args ...string) error

var command2Handler = map[string]commandHandler{
	string(networking.RunTrack): func(manager *networking.DeviceManager, args ...string) error {
		// args: <id> <fname>
		if len(args) < 2 {
			return fmt.Errorf("not enough arguments to call run_track")
		}

		deviceID, err := strconv.Atoi(args[0])
		if err != nil {
			return err
		}

		// check if device is connected
		_, found := manager.Devices()[uint(deviceID)]
		if !found {
			return fmt.Errorf("device %d not found", deviceID)
		}

		trackName := strings.TrimSpace(args[1])

		return manager.PlayTrack(uint(deviceID), networking.Track{
			Fname: trackName,
			Name:  "unknown",
		})
	},

	"clear": func(manager *networking.DeviceManager, args ...string) error {
		print("\033[H\033[2J")
		return nil
	},

	"devices": func(manager *networking.DeviceManager, args ...string) error {
		var deviceIDs []string
		for id := range manager.Devices() {
			deviceIDs = append(deviceIDs, fmt.Sprint(id))
		}

		fmt.Println(strings.Join(deviceIDs, " "))

		return nil
	},

	"tracks": func(manager *networking.DeviceManager, args ...string) error {
		if len(args) < 1 {
			return fmt.Errorf("not enough arguments to run tracks command")
		}

		switch args[0] {
		case "add":
			return manager.AddTracksFromDir(filePath)
		}

		return fmt.Errorf("invalid argument %s", args[0])
	},

	"download": func(manager *networking.DeviceManager, args ...string) error {
		// args: <deviceID> <fname>
		if len(args) < 2 {
			return fmt.Errorf("not enough arguments to call run_track")
		}

		deviceID, err := strconv.Atoi(args[0])
		if err != nil {
			return err
		}

		fname := args[1]

		return manager.RunCommand(uint(deviceID), networking.DownloadTrackCommand(
			strings.Join([]string{"http://", ip, ":3000/", fname}, ""),
		))
	},
}
