package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"strings"

	externalip "github.com/glendc/go-external-ip"
	"gitlab.com/deniskamazur/remuchi/networking"
)

var (
	ip          string
	address     string
	filePath    string
	serveTracks bool
)

func init() {
	flag.StringVar(&ip, "ip", "", "ip, uses external by default")
	flag.StringVar(&address, "address", ":12345", "server port")
	flag.StringVar(&filePath, "filePath", "tracks/", "track directory, creates if non-existant")
	flag.BoolVar(&serveTracks, "serveTracks", true, "run http server on :3000 with static files")

	if ip == "" {
		consensus := externalip.DefaultConsensus(nil, nil)
		ipL, err := consensus.ExternalIP()
		if err != nil {
			panic(err)
		}

		ip = ipL.String()
	}

	flag.Parse()
}

func main() {
	manager, err := networking.StartServer(address, filePath, serveTracks)
	if err != nil {
		panic(err)
	}

	for i := range manager.Devices() {
		log.Println(i)
	}

	for {
		//fmt.Print("> ")

		reader := bufio.NewReader(os.Stdin)
		message, _ := reader.ReadString('\n')

		tokens := strings.Split(strings.TrimSpace(message), " ")

		if len(tokens[0]) > 0 {
			command, found := command2Handler[tokens[0]]
			if !found {
				fmt.Printf("error: command %s not found\n\n", tokens[0])
			} else {
				err := command(manager, tokens[1:]...)
				if err != nil {
					log.Printf("error: %v\n", err)
				}
			}
		}
	}
}
