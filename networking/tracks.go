package networking

import (
	"io/ioutil"
	"os"
)

// track are stored on the device and server side
// tracks are identified by their filename

// Track specifies track data
type Track struct {
	Name  string `json:"name,omitempty"`
	Fname string `json:"fname"`
}

// Equal compares two track by their fname
func (t Track) Equal(track Track) bool {
	return t.Fname == track.Fname
}

// TrackStorage what you read is what you get, it stores track data
type TrackStorage map[string]*Track

// Exists checks if track is in storage
func (storage *TrackStorage) Exists(track Track) bool {
	_, found := (*storage)[track.Fname]
	return found
}

// Add ads track to track storage, returns false if file already in storage
func (storage *TrackStorage) Add(track Track) bool {
	if storage.Exists(track) {
		return false
	}

	(*storage)[track.Fname] = &track

	return true
}

// TrackStorageFromDir creates a file storage from given directory
func TrackStorageFromDir(path string) (TrackStorage, error) {
	files, err := ioutil.ReadDir(path)
	if err != nil {
		return TrackStorage{}, err
	}

	storage := make(TrackStorage)
	for _, finfo := range files {
		if !finfo.IsDir() {
			// TODO: check if it is an audio file
			storage.Add(Track{
				Name:  "unspecified",
				Fname: finfo.Name(),
			})
		}
	}

	return storage, nil
}

// EnsureDirectory ensures that directory exists
func EnsureDirectory(path string) {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		os.Mkdir(path, os.ModePerm)
	}
}
