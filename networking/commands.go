package networking

import "encoding/json"

// CommandName command string identificator
type CommandName string

// default command names
const (
	RunTrack      CommandName = "run_track"       // run track commands device to run a specific track
	SetDeviceInfo CommandName = "set_device_info" // set device info commands the device to set it's data
	DownloadTrack CommandName = "download_track"  // download track commands device to download a specific track
)

// Commands contains all commands
var Commands = [...]CommandName{RunTrack, SetDeviceInfo, DownloadTrack}

// Command is used for manager <-> device communication
type Command struct {
	Name CommandName `json:"name"`
	Info interface{} `json:"info"`
}

// JSON marshals command
func (c Command) JSON() ([]byte, error) {
	bytes, err := json.Marshal(c)
	return bytes, err
}

// RunTrackCommand creates a run track command from track data
func RunTrackCommand(track Track) Command {
	return Command{
		Name: RunTrack,
		Info: track,
	}
}

// DownloadTrackCommand creates a download track command
func DownloadTrackCommand(url string) Command {
	return Command{
		Name: DownloadTrack,
		Info: url,
	}
}
