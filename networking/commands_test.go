package networking

import (
	"encoding/json"
	"testing"
)

func TestCommands(t *testing.T) {
	command := Command{
		Name: "play_track",
		Info: Track{
			Fname: "gachi.mp3",
		},
	}

	bytes, err := json.Marshal(command)
	if err != nil {
		t.Error(err)
	}

	t.Logf("marshaled: %s", string(bytes))
}
