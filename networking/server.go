package networking

import (
	"encoding/json"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"path"
)

const staticPort = ":3000"

// Device describes device data
type Device struct {
	ID   uint
	Name string

	socket net.Conn

	Commands chan Command
}

// RunCommand send commands to device
func (device *Device) RunCommand(command Command) error {
	bytes, err := command.JSON()
	if err != nil {
		return err
	}

	_, err = device.socket.Write(bytes)

	return err
}

// receive recieves commands from socket and writes them into command channel
func (device *Device) receive() {
	for {
		message := make([]byte, 4096)
		length, err := device.socket.Read(message)
		if err != nil {
			device.socket.Close()
			close(device.Commands)
			break
		}
		if length > 0 {
			var command Command
			if err := json.Unmarshal(message[:length], &command); err != nil {
				log.Println(err)
			}

			device.Commands <- command
		}
	}
}

// DeviceManager manages devices
type DeviceManager struct {
	devices map[uint]*Device
	tracks  TrackStorage

	register chan net.Conn
}

// Devices returns all devices
func (manager *DeviceManager) Devices() map[uint]*Device {
	return manager.devices
}

func (manager *DeviceManager) start() {
	for conn := range manager.register {
		deviceID := uint(len(manager.devices))

		manager.devices[deviceID] = &Device{
			ID:     deviceID,
			Name:   "unset",
			socket: conn,
		}
	}
}

func (manager *DeviceManager) serveFiles(path string) {
	fs := http.FileServer(http.Dir(path))
	http.Handle("/", fs)

	log.Println("Started http file server")
	http.ListenAndServe(staticPort, nil)
}

// RunCommand runs command on specified machine
func (manager *DeviceManager) RunCommand(deviceID uint, command Command) error {
	device, found := manager.devices[deviceID]
	if !found {
		return fmt.Errorf("deviceID %d not found", deviceID)
	}

	return device.RunCommand(command)
}

// PlayTrack plays track on given device
func (manager *DeviceManager) PlayTrack(deviceID uint, track Track) error {
	device, found := manager.devices[deviceID]
	if !found {
		return fmt.Errorf("deviceID %d not found", deviceID)
	}

	if !manager.tracks.Exists(track) {
		return fmt.Errorf("track %s not found :(", track.Fname)
	}

	return device.RunCommand(RunTrackCommand(track))
}

// AddTracksFromDir adds tracks to storage from given directory
func (manager *DeviceManager) AddTracksFromDir(filepath string) error {
	tracks, err := TrackStorageFromDir(filepath)
	manager.tracks = tracks

	return err
}

// StartServer starts device manager server
func StartServer(address string, filePath string, serveTracks bool) (*DeviceManager, error) {
	manager := DeviceManager{
		devices:  make(map[uint]*Device),
		register: make(chan net.Conn),
	}

	// if file path not specified, use default
	if filePath == "" {
		currPath, err := os.Getwd()
		if err != nil {
			return nil, err
		}

		filePath = path.Join(currPath, "/tracks")
	}

	EnsureDirectory(filePath)

	// create track storage from directory
	storage, err := TrackStorageFromDir(filePath)
	if err != nil {
		return nil, err
	}

	manager.tracks = storage

	if serveTracks {
		go manager.serveFiles(filePath)
	}

	// networking stuff
	log.Printf("Listening on %s", address)
	listener, err := net.Listen("tcp", address)
	if err != nil {
		return nil, err
	}

	// start connection managing
	go manager.start()

	go func() {
		for {
			connection, err := listener.Accept()
			if err != nil {
				log.Println(err)
			}

			manager.register <- connection

			log.Println("recieved connection from ", connection.LocalAddr().Network())
		}
	}()

	return &manager, nil
}

// StartDevice starts device
func StartDevice(address string) *Device {
	log.Printf("Starting client on %s", address)
	connection, err := net.Dial("tcp", address)
	if err != nil {
		panic(err)
	}

	device := &Device{
		ID:       0,
		Name:     "unknown",
		socket:   connection,
		Commands: make(chan Command),
	}

	go device.receive()

	return device
}
