# remuchi

Ultimate audio pranking software

## Install
```bash
$ go install gitlab.com/deniskamazur/remuchi/...
```

## Docker incructions

```bash
$ docker build . --tag=remuchi
```

```bash
$  docker run -a stdin -a stdout -p 12345:12345 -p 3000:3000 -i -t remuchi
```
