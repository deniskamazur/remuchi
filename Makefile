PROJECT_NAME := remuchi
PKG := gitlab.com/deniskamazur/$(PROJECT_NAME)

dep: ## Get the dependencies
	@go get -v -d ./...

build:
	@go install -i -v $(PKG)/cmd/app
	@go install -i -v $(PKG)/cmd/server-cli
