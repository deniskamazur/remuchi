FROM golang:latest

ENV PROJECTDIR=$GOPATH/src/gitlab.com/deniskamazur/remuchi/

EXPOSE 3000
EXPOSE 12345
ADD . $PROJECTDIR

WORKDIR $PROJECTDIR

RUN make dep
RUN make build

ENTRYPOINT [ "server-cli" ]